import { Outlet } from 'react-router-dom';
import { useWhoIsAuthorize } from '../../contexts/AuthorizationContext';

const App = () => {
  const whoIsAuthorize = useWhoIsAuthorize();

  return (
    <>
      {whoIsAuthorize ? null : <Outlet/>}
    </>
  );
}

export default App;