import { useEffect, useState, useRef } from "react";
import { useNavigate, Link } from "react-router-dom";
import useNotification from "../../hooks/useNotification";
import { useSetWhoIsAuthorize } from "../../contexts/AuthorizationContext";
import useFetch from "../../hooks/useFetch";
import useAnimationSetterList from "../../hooks/useAnimationSetterList";
import Cookies from "universal-cookie"
import SummaryTransaction from "../shared/SummaryTransaction/SummaryTransaction";
import Loader from "../shared/Loader/Loader";
import CreditCard from "../shared/CreditCard/CreditCard";

const CURRENCIES = {
  crc: {code: 'CRC', symbol: '₡'},
  usd: {code: 'USD', symbol: '$'}
}

const CardPanel = props => {
    const block = 'card-panel';
    const { account, design, position } = props;
    const navigate = useNavigate();
    const setWhoIsAuthorize = useSetWhoIsAuthorize();
    const { notification$ } = useNotification();
    const [currBalance, setCurrBalance] = useState(0);
    const [ lastestTransacts, setLatestTransacts ] = useState([]);
    const transactItemRefs = useRef([]);
    transactItemRefs.current = [];
    const [ data, error, sendRequest, pending ] = useFetch(
      process.env.REACT_APP_API_URL + `/transactions/${account.number}?limit=3`,
      false,
      {
        credentials: "include",
      }
    );

    const play = useAnimationSetterList(transactItemRefs.current, `${block}__latest-transact-section__transact--display`, 250);

    useEffect(() => {
      sendRequest();
      // eslint-disable-next-line
    }, []);

    useEffect(() => {
      if(error) {
        if(error.name === 'TypeError') {
            notification$.next({
                type: 'error',
                message: "Couldn't communicate with the server, try again later."
            });
        } else if(error.cause.status === 401) {
            if(error.cause.name === 'InvalidTokenError') {
              const cookies = new Cookies();
              cookies.remove('auth_token');
              notification$.next({
                type: 'error',
                message: "Session expired. Log in again"
              });
            } else {
              notification$.next({
                type: 'error',
                message: "You must be logged in to access this page"
              });
            }
            setWhoIsAuthorize(null);
            navigate('/');
        } else if (error.cause.status === 404) {
            navigate('/user/notfound');
        } else {
            notification$.next({
                type: 'error',
                message: 'The server could not process the request, please try again later.'
            });
        }
      } else if (data) {
        setLatestTransacts(data.data);
        play();
      }
      // eslint-disable-next-line
    }, [data, error]);

    useEffect(() => {
      let timerId;
      if(currBalance !== account.balance) {
        const increment = Math.round(account.balance/25);
        timerId = setTimeout(() => {
          if (currBalance + increment <= account.balance) {
            setCurrBalance(curr => curr + increment);
          } else {
            setCurrBalance(account.balance);
          }
        }, 10 + (currBalance/account.balance)*50);
      }
      return () => {
        timerId && clearTimeout(timerId);
      }
      // eslint-disable-next-line
    }, [currBalance, account]);

    const addToRefs = (element) => {
      if(element && !transactItemRefs.current.includes(element)) {
        transactItemRefs.current.push(element);
      }
    };
    
    const transactsSummaries = lastestTransacts.map(transact =>
        <div key={transact.id}
        className={`${block}__latest-transact-section__transact`}
        ref={addToRefs}>
          <SummaryTransaction 
          accountNumber={account.number} 
          symbol={CURRENCIES[account.currency].symbol}
          transaction={transact}
          />
        </div>
    );

    return (
      <div className={`${block}__root`}>
        <div className={`${block}__panel`}>
            <div className={`${block}__card-wrp
            ${ position === 'left' ? block + '__card-wrp--left' : block + '__card-wrp--right'}`}>
              <CreditCard accountNumber={account.number} 
              currencyCode={CURRENCIES[account.currency].code}
              design={design}/>
            </div>
            <div className={`${block}__body`}>
              <div className={`${block}__balance-section`}>
                <h3 className={`${block}__balance-section__title`}>Balance:</h3>
                <data className={`${block}__balance-section__value`}>{CURRENCIES[account.currency].symbol}{currBalance}</data>
              </div>
              <div className={`${block}__latest-transact-section`}>
              <h3 className={`${block}__latest-transact-section__title`}>Latest transactions:</h3>
              <Link to={`/user/history/${account.number}`} 
              className={`${block}__latest-transact-section__btn`}
              aria-label={`Go to complete history page of account with number ${account.number}`}>Full history</Link>  
                { (pending && <Loader/>) || 
                  (!transactsSummaries.length && <p className={`${block}__latest-transact-section__no-data`}>No transactions found for this account</p>) || 
                  <>{transactsSummaries}</>
                }
              </div>
            </div>
        </div>
      </div>
  )
}

export default CardPanel;