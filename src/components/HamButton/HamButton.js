const HamButton = props => {
    const { opened, onClick } = props;
    const block = 'ham-button';

    return (
        <button className={`${block}__root ${opened ? block + '__root--opened':''}`} 
        onClick={onClick}
        aria-label={opened ? 'Close navigation menu' : 'Open navigation menu'}>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </button>
    );
}

export default HamButton