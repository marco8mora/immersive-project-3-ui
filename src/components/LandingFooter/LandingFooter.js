import SmallLogo from "../shared/SmallLogo/SmallLogo";
import { BsFacebook, BsLinkedin } from "react-icons/bs";
import { AiFillTwitterCircle } from "react-icons/ai";


const LandingFooter = () => {
    const block = 'landing-footer';
    
    return (
        <footer className={`${block}__root`}>
            <div className={`${block}__logo-section`}>
                <SmallLogo/>
            </div>
            <div className={`${block}__copyright`}>
                <p>© 2022 Crown & Shield Bank. All rights reserved.</p>
            </div>
            <div className={`${block}__socials-section`}>
                <h3 className={`${block}__socials-section__title`}>Our Socials:</h3>
                <ul className={`${block}__socials-section__list`}>
                    <li>
                        <a href="https://www.google.com/" aria-label="Go to Crown & Shield Bank's Facebook page">
                            <BsFacebook/>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com/" aria-label="Go to Crown & Shield Bank's Twitter page">
                            <AiFillTwitterCircle/>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com/" aria-label="Go to Crown & Shield Bank's LinkedIn page">
                            <BsLinkedin/>
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    );
}

export default LandingFooter