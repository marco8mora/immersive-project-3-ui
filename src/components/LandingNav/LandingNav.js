import useBreakpoint from '../../hooks/useBreakpoint';
import SmallLogo from '../shared/SmallLogo/SmallLogo';
import HamButton from '../HamButton/HamButton';
import { useEffect, useState } from 'react';
import useToggleBodyScroll from '../../hooks/useToggleBodyScroll';

const LandingNav = (props) => {
    const { sections, currSection } = props
    const block = 'landing-nav';
    const device = useBreakpoint();
    const toggleBodyScroll = useToggleBodyScroll();
    const [ showMenu, setShowMenu ] = useState(false);

    const toggleMenu = () => setShowMenu(curr => !curr);
    const closeMenu = () => setShowMenu(false);

    const showMobileMenu = (device === 'mobile' || device === 'tablet');

    useEffect(() => {
        if(showMenu) {
            toggleBodyScroll();
        } else {
            toggleBodyScroll()
        }
    // eslint-disable-next-line
    }, [showMenu])

    const menuLinks = sections.map((section, i) => 
        <li key={section.id}>
            <a 
            className={`${currSection === i ? 'active':''}`}
            href={`#${section.id}`} 
            aria-label={`Scroll to ${section.name} section`}
            onClick={closeMenu}>
                {section.name}
            </a>
        </li>
    );
    
    return (
        <>
            <nav className={`${block}__root`}>
                <div className={`${block}__logo-section`}>
                    <a href={`#${sections[0].id}`} aria-label='Go to top of page'>
                        <SmallLogo />
                    </a>
                </div>
                <div className={`${block}__menu-section`}>
                    {   ( showMobileMenu &&
                            <HamButton opened={showMenu} onClick={toggleMenu}/>
                        )
                        ||
                        <ul className={`${block}__horiz-menu`}>
                            {menuLinks}
                        </ul>
                    }
                </div>
            </nav>
            { showMobileMenu &&
                <div className={`${block}__vert-menu ${showMenu ? block + '__vert-menu--opened': ''}`}>
                    <ul className={`${block}__vert-menu__list`}>
                        {menuLinks}
                    </ul>
                </div>
            }
        </>
    );
}

export default LandingNav;