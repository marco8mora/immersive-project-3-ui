import { useEffect, useRef, useState } from "react";
import CostaRicaOutline from "../shared/CostaRicaOutline/CostaRicaOutline";
import useOnScreen from "../../hooks/useOnScreen";
import useAnimationSetterList from "../../hooks/useAnimationSetterList";
import useBreakpoint from "../../hooks/useBreakpoint";

const Locations = props => {
    const block = 'locations';
    const { locations } = props;
    const device = useBreakpoint();
    const mapRef = useRef(null);
    const locationItemRefs = useRef([]);
    locationItemRefs.current = [];
    const [ firstDisplay, setFirstDisplay ] = useState(true);
    const isVisible = useOnScreen(mapRef, 0.5);

    const play = useAnimationSetterList(locationItemRefs.current, `${block}__list__li--entered`, 500);

    useEffect(() => {
        let timerId;
        if(firstDisplay && isVisible) {
            play();
            timerId = setTimeout(() => {
                setFirstDisplay(false);
            }, 1000)
        }
        return () => clearTimeout(timerId);
     // eslint-disable-next-line   
    }, [isVisible, firstDisplay]);

    const addToRefs = (element) => {
        if(firstDisplay) {
            if(element && !locationItemRefs.current.includes(element)) {
                locationItemRefs.current.push(element);
            }
        }
    };

    const coordenates = [
        {mobile: [160,115], desktop: [330, 240]},
        {mobile: [170,115], desktop: [350, 240]},
        {mobile: [220,114], desktop: [440, 223]},
        {mobile: [70,80], desktop: [138, 154]},
        {mobile: [126,149], desktop: [254, 302]},
    ];

    const showMobile = (device === 'mobile') || (device === 'tablet')

    return (
        <div ref={mapRef} className={`${block}__root`}>

            <div  className={`${block}__map-container`}>        
                { isVisible &&
                    <CostaRicaOutline/>
                }
                {   coordenates.map(coord => { 
                        const coordX = showMobile ? coord.mobile[0] : coord.desktop[0];
                        const coordY = showMobile ? coord.mobile[1] : coord.desktop[1];
                        const style = {transform: `translate(${coordX}px,${coordY}px)`};
                        return <span key={''+coordX+coordY} className={`${block}__map-container__pin`}
                            style={style}
                        />    
                    }
                )}
            </div>
            <div className={`${block}__list-container`}>
                <h2 className={`${block}__list__title`}>Where are we located?</h2>
                <ul className={`${block}__list`}>
                    {
                        locations.map(loc => 
                            <li key={loc} ref={addToRefs} className={`${block}__list__li`}>
                                {loc}
                            </li>
                        )    
                    }
                </ul>
            </div>
        </div>
    );
}

export default Locations;