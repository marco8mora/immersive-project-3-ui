import { useEffect, useRef, useState } from "react";
import SlidingCoin from "../shared/Coin/Coin"
import useOnScreen from "../../hooks/useOnScreen";
import useBreakpoint from "../../hooks/useBreakpoint";
import InfoParagraph from "../shared/InfoParagraph/InfoParagraph";
import useAnimationSetter from "../../hooks/useAnimationSetter";

const OurServices = () => {
    const block = 'our-services';
    const sectRef = useRef(null);
    const info1 = useRef(null);
    const info2 = useRef(null);
    const info3 = useRef(null);
    const play = useAnimationSetter([
        { ref: info1, class: `${block}__info-anim-wrapper--display`, delay: 400},
        { ref: info2, class: `${block}__info-anim-wrapper--display`, delay: 1000},
        { ref: info3, class: `${block}__info-anim-wrapper--display`, delay: 1500},
    ]);
    const device = useBreakpoint();
    const [ firstDisplay, setFirstDisplay ] = useState(true);
    const isVisible = useOnScreen(sectRef, 0.5);

    useEffect(() =>{
        let timerId;
        if(firstDisplay && isVisible) {
            play();
            timerId = setTimeout(() => {
                setFirstDisplay(false);
            }, 3000)
        }
        return () => clearTimeout(timerId);
        // eslint-disable-next-line
    }, [isVisible, firstDisplay])

    const showMobile = device === 'mobile' || device === 'tablet';
    return (
        <div className={`${block}__root`} ref={sectRef}>
            <h2 className={`${block}__title`}>Why use Crown & Shield?</h2>
            <div className={`
            ${block}__anim-coin ` +
            `${(showMobile && isVisible && firstDisplay) ? block + '__anim-coin--vert': ''} ` +
            `${(!showMobile && isVisible && firstDisplay) ? block + '__anim-coin--horiz': ''}`
            }>
                <SlidingCoin/>
            </div>
            <div className={`${block}__info-section`}>
                <div ref={info1} className={`${block}__info-anim-wrapper`}>
                    <InfoParagraph 
                    title={'Best Interests Rates!'}
                    info={'We have the best interest in the market! Get a loan without worrying about high interests in the future and get that dream investment you have always wanted'}
                    textColor={'darkblue'}/>
                </div>
                <div ref={info2} className={`${block}__info-anim-wrapper`}>
                    <InfoParagraph 
                    title={'Instantaneous National & International Transfers!'}
                    info={'Transfer money quickly between national and international banks. Say goodbye to long waiting times to see your money transfered'}
                    textColor={'darkblue'}/>
                </div>
                <div ref={info3} className={`${block}__info-anim-wrapper`}>
                    <InfoParagraph 
                    title={'Pay Recurrent Services'}
                    info={'We offer a wide range of service payments through our bank. From governmental services like taxes, water and electrical bills, to private services like phone providers, streaming services and others'}
                    textColor={'darkblue'}/>
                </div>
            </div>
        </div>
    )
}

export default OurServices