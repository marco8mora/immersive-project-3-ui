import { useEffect, useRef, useState } from "react";
import Cookies from "universal-cookie";
import { Outlet, NavLink, useNavigate, useLocation } from "react-router-dom";
import { useWhoIsAuthorize, useSetWhoIsAuthorize } from "../../contexts/AuthorizationContext";
import useNotification from "../../hooks/useNotification";
import useBreakpoint from "../../hooks/useBreakpoint";
import SmallLogo from "../shared/SmallLogo/SmallLogo";
import HamButton from "../HamButton/HamButton";
import { UserDataProvider } from "../../contexts/UserDataContext";

const UserNav = () => {
    const block = 'user-nav';
    const whoIsAuthorize = useWhoIsAuthorize();
    const setWhoIsAuthorize = useSetWhoIsAuthorize();
    const navigate = useNavigate();
    const location = useLocation();
    const { notification$ } = useNotification();
    const device = useBreakpoint();
    const [ isClosed, setIsClosed ] = useState(true);
    const swiper = useRef(null);

    useEffect(() => {
        if(whoIsAuthorize === null) {
            notification$.next({
                type: 'error',
                message: "You must be logged in to access this page"
            });
            navigate('/');
        } else if (whoIsAuthorize === undefined) {
            navigate('/');
        }
    // eslint-disable-next-line
    }, [whoIsAuthorize]);

    useEffect(() => {
        let timer;
        if(swiper.current) {
            swiper.current.classList.add(`${block}__swiper--swipe`);
            timer = setTimeout(() => {
                swiper.current.classList.remove(`${block}__swiper--swipe`);
            }, 1000)
        }
        return () => timer && clearTimeout(timer);
    }, [location]);

    if(!whoIsAuthorize) {
        return null;
    }

    const toggleIsClosed = () => setIsClosed(curr => !curr);
    
    const logOut = () => {
        setWhoIsAuthorize(null);
        const cookies = new Cookies();
        cookies.remove('auth_token');
        notification$.next({
            type: 'success',
            message: 'Logged out'
        });
        navigate('/');
    }

    const renderLinks = (entries) => {
        return entries.map(entry => 
        <li key={entry.label}>
            <NavLink 
            to={entry.to} 
            onFocus={() => setIsClosed(false)}
            aria-label={entry.label}
            className={({ isActive }) => isActive ? `${block}__navlink ${block}__navlink--active` : `${block}__navlink`}
            >{entry.name}</NavLink>
        </li>
        );
    };

    const showMobile = (device === 'mobile' || device === 'tablet')

    return (
        <>
            <header>
                { showMobile &&
                    <div className={`${block}__ham-btn-cont ${isClosed ? '' : block + '__ham-btn-cont--open'}`}>
                        <HamButton opened={!isClosed} onClick={toggleIsClosed}/>
                    </div>
                }
                <nav className={`
                    ${block}__root 
                    ${(isClosed && showMobile) ? '' : block + '__root--open'}`}
                    onBlur={() => setIsClosed(true)}>
                    <div className={`${block}__logo-section`}>
                        <SmallLogo />
                    </div>
                    <ul className={`${block}__links-list`}>
                        {
                            renderLinks([
                                {to: '/user/dashboard', name: 'Dashboard', label: 'Go to dashboard page'},
                                {to: '/user/addmoney', name: 'Add Money', label: 'Go to add money page'},
                                {to: '/user/transfer', name: 'Transfer Money', label: 'Go to transfer money page'},
                                {to: '/user/services', name: 'Pay Services', label: 'Go to pay services page'}
                            ])
                        }
                    </ul>
                    <button 
                    className={`${block}__log-out-btn`} 
                    onClick={logOut}
                    onFocus={() => setIsClosed(false)}>Log out</button>
                </nav>
                </header>
            <main className={`${block}__main`}>
                <UserDataProvider>
                    <Outlet />
                </UserDataProvider>
                <div className={`${block}__swiper`} ref={swiper}></div>
            </main>
        </>
    );
}

export default UserNav;