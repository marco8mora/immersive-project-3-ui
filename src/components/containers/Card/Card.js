const Card = ({children}) => {
    const block = 'card';
    
    return (
        <div className={`${block}__root`}>
            {children}
        </div>
    )
}

export default Card;