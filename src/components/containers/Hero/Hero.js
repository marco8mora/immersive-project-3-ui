import { useEffect, useState } from "react";
import { AiOutlinePauseCircle, AiOutlinePlayCircle } from "react-icons/ai"

const Hero = props => {
    const block = 'hero';
    const {children, images, timeInterval} = props;
    const [ currImage, setCurrImage ] = useState(0);
    const [ paused, setPaused ] = useState(false);

    useEffect(() => {
        if (!paused) {
            const enterTimerId = setTimeout(() => {
            setCurrImage(curr => (curr+1)%images.length);
            }, timeInterval);

            return () => {
                clearTimeout(enterTimerId);
            };
        }
    }, [images, currImage, timeInterval, paused]);

    return (
        <div className={`${block}__root`}>
            <div className={`${block}__image-caroussel`}>
                {   
                    images.map((image, index) => 
                    <div key={index} className={
                        `${block}__image-caroussel__image  
                        ${index===currImage ? block + '__image-caroussel__image--current': ''}
                        ${(index===(currImage-1) || (index===(images.length-1) && currImage===0)) ? block + '__image-caroussel__image--previous': ''}`
                    
                    }
                    style={{backgroundImage: `linear-gradient(rgba(10, 18, 42, 0.4), rgba(10, 18, 42, 0.4)), url(${image})`}}
                    ></div>)
                }
            </div>
            <div className={`${block}__content`}>
                <button className={`${block}__pause-btn`}
                aria-label={paused ? 'Play hero carousel' : 'Pause hero carousel'}
                onClick={() => setPaused(curr => !curr)}
                >
                    {paused ? <AiOutlinePlayCircle/> : <AiOutlinePauseCircle/>}
                </button>
                {children}
            </div>
        </div>
    );
}

export default Hero;