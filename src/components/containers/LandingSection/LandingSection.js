const LandingSection = props => {
    const block = 'landing-section';
    const { children, bgColor } = props

    return (
        <div className={`${block}__root ${block}__root--${bgColor}`}>
            {children}
        </div>
    )
}

export default LandingSection;