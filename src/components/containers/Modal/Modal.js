import { useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import useToggleBodyScroll from '../../../hooks/useToggleBodyScroll';
import { IoIosClose } from 'react-icons/io';

const Modal = props => {
    const block = 'modal';
    const {open, children, onClose} = props;
    const toggleBodyScroll = useToggleBodyScroll();
    const rootRef = useRef(null);

    useEffect(() => {
        if(open) {
            rootRef.current.focus();
            toggleBodyScroll();
        }
        // eslint-disable-next-line
    }, []);

    if(!open) return null;
    
    return ReactDOM.createPortal(
        <>
            <div className={`${block}__overlay`}/>
            <div className={`${block}__root`}
            tabIndex='0'
            aria-label='Modal appeared on screen'
            ref={rootRef}>
                <button className={`${block}__close-btn`} 
                onClick={onClose}
                aria-label='Close modal'>
                    <IoIosClose/>
                </button>
                {children}
            </div>
        </>
    , document.getElementById('modal'));
}

export default Modal;