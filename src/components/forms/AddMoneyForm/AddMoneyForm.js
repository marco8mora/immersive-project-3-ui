import TextInput from "../TextInput/TextInput";
import DropdownSelect from "../DropdownSelect/DropdownSelect";
import useNotification from "../../../hooks/useNotification";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import useFetch from "../../../hooks/useFetch";
import Cookies from "universal-cookie";
import { useSetWhoIsAuthorize } from "../../../contexts/AuthorizationContext";
import Loader from "../../shared/Loader/Loader";

const AddMoneyForm = props => {
    const block = 'add-money-form';
    const { accounts } = props;
    const navigate = useNavigate();
    const setWhoIsAuthorize = useSetWhoIsAuthorize();
    const [ formData, setFormData ] = useState({
        account_credited : accounts[0].number,
        description: ''
    });
    const [ formErrors, setFormErrors ] = useState({});
    const { notification$ } = useNotification();
    const [ data, error, sendRequest, pending ] = useFetch(
        process.env.REACT_APP_API_URL + '/transactions/addmoney',
        false,
        {
            method: 'POST',
            credentials: 'include',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(formData)
        }
    );
    
    const onChangeDropdown = (value) => {
        setFormData(curr => {return {
            ...curr,
            account_credited: value
        }})
    };

    const validateFields = () => {
        let errors = {};
        const externalAccRegex = /^[A-Z]{2}[0-9]{20}$/;
        if(!externalAccRegex.test(formData.external_account)) {
            errors.external_account = true;
        }
        const amountRegex = /^[+-]?\d+(\.\d+)?$/;
        if(!amountRegex.test(formData.amount)) {
            errors.amount = true;
        }
        if(Number(formData.amount) < 0) {
            errors.amount = true;
        }
        if(!formData.description.length) {
            errors.description = true;
        }

        return errors
    }

    const onChange = (event) => {
        const value = event.target.value;
        setFormData({
            ...formData,
            [event.target.name]: value
        });
        setFormErrors({
            ...formErrors,
            [event.target.name]: false
        });
    };

    const onSubmit = (event) => {
        event.preventDefault();
        const errors = validateFields();
        if(!Object.keys(errors).length) {
            formData.amount = Number(formData.amount);
            sendRequest();
        } else {
            setFormErrors(errors);
        }
    }

    useEffect(() => {
        if(error) {
            console.log(error);
            if(error.name === 'TypeError') {
                notification$.next({
                    type: 'error',
                    message: "Couldn't communicate with the server, try again later."
                })
            } else if(error.cause.status === 401) {
                if(error.cause.name === 'InvalidTokenError') {
                  const cookies = new Cookies();
                  cookies.remove('auth_token');
                  notification$.next({
                    type: 'error',
                    message: "Session expired. Log in again"
                  });
                } else {
                  notification$.next({
                    type: 'error',
                    message: "You must be logged in to access this page"
                  });
                }
                setWhoIsAuthorize(null);
                navigate('/');
            } else if(error.cause.name === 'DataFormatError') {
                
            } else {
                notification$.next({
                    type: 'error',
                    message: 'The server could not process the request, please try again later.'
                });
            }
        } else if (data) {
            notification$.next({
                type: 'success',
                message: 'Transaction completed successfully'
            });
            navigate('/user/dashboard');
        }
        // eslint-disable-next-line
    }, [data, error]);

  return (
    <div className={`${block}__root`}>
        <form method="post" className={`${block}__form`}>
            {  (pending && <Loader/>) ||
                <>
                    <TextInput name={'external_account'} 
                    label={'Origin account'} 
                    type={'text'} 
                    placeholder={'CR00000000000000000000'}
                    onChange={onChange}
                    hasError={formErrors.external_account}
                    errorMessage={'Please enter a valid IBAN account'}/>
                    <TextInput name={'amount'} 
                    label={'Amount to add'} 
                    type={'number'} 
                    placeholder={'0.0'}
                    onChange={onChange}
                    hasError={formErrors.amount}
                    errorMessage={'Please enter a valid amount'}/>
                    <TextInput name={'description'} 
                    label={'Description'} 
                    type={'text'} 
                    placeholder={'Enter a description'}
                    onChange={onChange}
                    hasError={formErrors.description}
                    errorMessage={'Please a description for the transaction'}/>
                    <DropdownSelect value={formData.account_credited ? formData.account_credited : accounts[0].number}
                    options={
                        accounts.map(account => 
                            { return {
                                label: account.number, 
                                value: account.number
                            }})
                    }
                    label={'Destination account'}
                    name={'destination_account'}
                    onChange={onChangeDropdown}/>
                    <button type="submit" onClick={event => onSubmit(event)} className={`${block}__submit-btn`}>
                        Submit
                    </button>
                </>
            }
        </form>
    </div>
  )
}

export default AddMoneyForm;