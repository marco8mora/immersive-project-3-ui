const DateInput = props => {
    const block = "date-input";
    const { name, label, onChange, initialValue } = props;
    
    return (
        <div className={`${block}__root`}>
            <label id={`${name}Label`} htmlFor={`${name}Input`} className={`${block}__label`}>{label}</label>
            <div className={`${block}__input-container`}>
                <input
                id={`${name}Input`} 
                type="date" 
                name={name}
                aria-labelledby={`${name}Label`}
                className={`${block}__input`}
                onChange={event => onChange(event)}
                defaultValue={initialValue}
                />
            </div>
        </div>
    )
}

export default DateInput;