import { useEffect, useState } from "react";
import useFetch from "../../../hooks/useFetch";
import useNotification from "../../../hooks/useNotification";
import DateInput from "../DateInput/DateInput";
import Cookies from "universal-cookie";
import { useSetWhoIsAuthorize } from "../../../contexts/AuthorizationContext";
import { useNavigate } from "react-router-dom";

const DateRangeForm = props => {
    const block = 'date-range-form';
    const { notification$ } = useNotification();
    const setWhoIsAuthorize = useSetWhoIsAuthorize();
    const navigate = useNavigate();

    const { setList, account, closeModal } = props;
    const [ formData, setFormData ] = useState({});
    
    const [ data, error, sendRequest ] = useFetch(
        process.env.REACT_APP_API_URL + `/transactions/${account}?limit=99&from=${formData.from}&to=${formData.to}`,
        false,
        {
            credentials: 'include'
        }
    )

    const onChange = (event) => {
        const value = event.target.value;
        setFormData({
            ...formData,
            [event.target.name]: value
        });
    };

    const onSubmit = (event) => {
        event.preventDefault();
        sendRequest();
    };
    
    useEffect(() => {
        if(error) {
            if(error.name === 'TypeError') {
                notification$.next({
                    type: 'error',
                    message: "Couldn't communicate with the server, try again later."
                });
            } else if(error.cause.status === 401) {
                if(error.cause.name === 'InvalidTokenError') {
                    const cookies = new Cookies();
                    cookies.remove('auth_token');
                    notification$.next({
                    type: 'error',
                    message: "Session expired. Log in again"
                    });
                } else {
                    notification$.next({
                    type: 'error',
                    message: "You must be logged in to access this page"
                    });
                }
                setWhoIsAuthorize(null);
                navigate('/');
            } else if (error.cause.status === 404) {
                navigate('/user/notfound');
            } else {
                notification$.next({
                    type: 'error',
                    message: 'The server could not process the request, please try again later.'
                });
            }
            } else if (data) {
                setList(data.data);
                closeModal();
            }
            // eslint-disable-next-line
    }, [data, error]);

    return (
    <>
        <h3 className={`${block}__title`}>Select a date range</h3>
        <form method="post"
        className={`${block}__form`}>
            <DateInput name={'from'} label={'Starting at'} 
            onChange={onChange}
            initialValue={new Date().toLocaleDateString()}/>
            <DateInput name={'to'} label={'Ending at'} 
            onChange={onChange}
            initialValue={new Date().toLocaleDateString()}/>
            <button type="submit" 
            className={`${block}__submit-btn`}
            onClick={(event) => onSubmit(event)}>Submit</button>
        </form>
    </>
    )
}

export default DateRangeForm;