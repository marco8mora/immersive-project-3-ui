import { useState } from "react";

const DropdownSelect = props => {
    const block = 'dropdown-select';
    const { label, value, name, options, onChange } = props;
    const [ isOpen, setIsOpen ] = useState(false);

    const renderOptions = options.map(option => 
    <li key={option.value}>
        <button
        type='button'
        className={`${block}__dropdown-list__btn`}
        onClick={()=>{
            onChange(option.value)
            setIsOpen(false);
        }}
        >
            {option.value}
        </button>
    </li>)

    return (
        <div className={`${block}__root`}>
            <label id={`${name}Label`} htmlFor={`${name}Dropdown`} className={`${block}__label`}>{label}</label>
            <button type='button' className={`${block}__dropdown-btn`} 
            id={`${name}Dropdown`}
            aria-labelledby={`${name}Label`}
            data-bs-toggle='dropdown' aria-expanded={isOpen ? 'true':'false'}
            aria-label={`Open ${name} dropdown`}
            onClick={()=>{setIsOpen(curr => !curr)}}>
                {value}
            </button>
            {  isOpen &&
                <ul className={`${block}__dropdown-list`}>
                    {renderOptions}
                </ul>
            }
        </div>
    )
}

export default DropdownSelect