import SimpleFileUpload from "react-simple-file-upload";

const FileInput = props => {
    const block = 'file-input';
    const { name, label, onUpload } = props;

    return (
        <div className={`${block}__root`}>
            <label id={`${name}Label`} htmlFor={`${name}Input`} className={`${block}__label`}>{label}</label>
            <div className={`${block}__dropzone-wrapper`}>
                <span className={`${block}__dropzone-cont`}>
                    <SimpleFileUpload
                        id={`${name}Input`}
                        apiKey={process.env.REACT_APP_SFU_API_KEY}
                        onSuccess={onUpload}
                        preview='true'
                        width={'125'}
                        height={'125'}
                    />
                </span>
            </div>
        </div>
    )
}

export default FileInput;