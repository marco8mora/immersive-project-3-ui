import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Cookies from 'universal-cookie';
import { useSetWhoIsAuthorize } from "../../../contexts/AuthorizationContext";
import useNotification from "../../../hooks/useNotification";
import useFetch from "../../../hooks/useFetch";
import TextInput from "../TextInput/TextInput";
import Loader from "../../shared/Loader/Loader";

const LoginForm = () => {
    const block = 'login-form';
    const navigate = useNavigate();
    const { notification$ } = useNotification();
    const setWhoIsAuthorize = useSetWhoIsAuthorize();
    const [ formData, setFormData ] = useState({});
    const [ hasError, setHasError ] =useState(false);
    const [ data, error, sendRequest, pending ] = useFetch(
        process.env.REACT_APP_API_URL + '/login',
        false,
        {
            method: 'POST',
            credentials: "include",
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Credentials': 'true'
            },
            body: JSON.stringify(formData)
        }
    )

    const onChange = (event) => {
        setHasError(false);
        const value = event.target.value;
        setFormData({
            ...formData,
            [event.target.name]: value
        });
    };

    const onSubmit = (event) => {
        event.preventDefault();
        sendRequest();
    }

    useEffect(() => {
        if(error) {
            if(error.name === 'TypeError') {
                notification$.next({
                    type: 'error',
                    message: "Couldn't communicate with the server, try again later."
                });
            } else if(error.cause.status === 400) {
                setHasError(true);
            } else {
                notification$.next({
                    type: 'error',
                    message: 'The server could not process the request, please try again later.'
                });
            }
        } else if (data) {
            const cookies = new Cookies();
            cookies.set('auth_token', data.data, {
                maxAge: process.env.REACT_APP_JWT_EXPIRING_TIME
            });
            setWhoIsAuthorize(data.data);
            navigate('/user/dashboard');
        }
    // eslint-disable-next-line
    }, [data, error])

    return (
        <div className={`${block}__root`}>
            <h2 className={`${block}__title`}>Log In</h2>
            <div className={`${block}__login-section`}>

                {   
                    (pending && <Loader/>)
                    ||
                    <form method="post" className={`${block}__form`}>
                        <TextInput
                        name={'email'}
                        label={'Email'}
                        type={'text'}
                        placeholder={'Input your email'}
                        onChange={onChange}
                        hasError={hasError}
                        />
                        <TextInput
                        name={'password'}
                        label={'Password'}
                        type={'password'}
                        placeholder={'Input your password'}
                        onChange={onChange}
                        hasError={hasError}
                        />
                        {   hasError &&
                            <span className={`${block}__error`}>Invalid login credentials</span>
                        }
                        <button type="submit" onClick={onSubmit} className={`${block}__submit-btn`}>
                            Log In
                        </button>
                    </form>
                }
            </div>
            <div className={`${block}__signup-section`}>
                <h2 className={`${block}__signup-title`}>Don't have an account?</h2>
                <Link className={`${block}__signup-link`} to='/signup' aria-label='Go to sign up page'>Sign Up</Link>
            </div>
        </div>
    )
}

export default LoginForm;