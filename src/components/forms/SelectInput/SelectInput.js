const SelectInput = props => {
    const block = "select-input";
    const { options, name, label, onChange } = props;

    return (
        <div className={`${block}__root`}>
            <label id={`${name}Label`} htmlFor={`${name}Input`} className={`${block}__label`}>{label}</label>
            <span className={`${block}__input-container`}>
                <select
                id={`${name}Input`} 
                name={name}
                aria-labelledby={`${name}Label`}
                className={`${block}__input`}
                onChange={event => onChange(event)}
                defaultValue={options[0].value}>
                    {
                        options.map(option => 
                            <option key={option.value} 
                            value={option.value}>
                                {option.name}
                            </option>
                        )
                    }
                </select>
            </span>
        </div>
    )
}

export default SelectInput;