import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import useNotification from "../../../hooks/useNotification";
import useFetch from "../../../hooks/useFetch";
import FileInput from "../FileInput/FileInput";
import SelectInput from "../SelectInput/SelectInput";
import TextInput from "../TextInput/TextInput";
import Loader from "../../shared/Loader/Loader";

const SignUpForm = () => {
    const block = 'signup-form';
    const { notification$ } = useNotification();
    const navigate = useNavigate();
    const [ formData, setFormData ] = useState({});
    const [ formErrors, setFormErrors ] = useState({});
    const [ data, error, sendRequest, pending ] = useFetch(
        process.env.REACT_APP_API_URL + '/signup',
        false,
        {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(formData)
        }
    )
    
    const onChange = (event) => {
        const value = event.target.value;
        setFormData({
            ...formData,
            [event.target.name]: value
        });
        setFormErrors({
            ...formErrors,
            [event.target.name]: false
        });
    };

    const errorMessages = {
        full_name: 'Required',
        id: 'Please enter a valid ID number',
        email: 'Please enter a valid email',
        password: 'Please enter a valid password',
        confirm: 'Passwords don\'t match'
    }
    
    const onFileUpload = (url) => {
        formData.photo_url = url;
    }

    const validateFields = () => {
        let errors = {};
        if(!formData.full_name || !formData.full_name.length) {
            errors.full_name = true;
        }
        const idRegex = /^\d{9}$/;
        if(!idRegex.test(formData.id)) {
            errors.id = true;
        }
        const emailRegex = /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if(!emailRegex.test(formData.email)) {
            errors.email = true;
        }
        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        if(!passwordRegex.test(formData.password)) {
            errors.password = true;
        }
        if(formData.password !== formData.confirm) {
            errors.confirm = true;
        }

        return errors
    }

    const onSubmit = (event) => {
        event.preventDefault();
        const errors = validateFields();
        if(!Object.keys(errors).length) {
            sendRequest();
        } else {
            setFormErrors(errors);
        }
    }

    useEffect(() => {
        if(error) {
            if(error.name === 'TypeError') {
                notification$.next({
                    type: 'error',
                    message: "Couldn't communicate with the server, try again later."
                })
            } else if(error.cause.name === 'DataFormatError') {
                notification$.next({
                    type: 'error',
                    message: "Please fill ALL the inputs correctly."
                });
            } else if(error.cause.name === 'UniqueDataError') {
                notification$.next({
                    type: 'error',
                    message: `Value for field ${Object.keys(error.cause.data)[0] === 'id' ? 'ID number' : 'email'} already registered, please type another one.`
                });
            } else {
                notification$.next({
                    type: 'error',
                    message: 'The server could not process the request, please try again later.'
                });
            }
        } else if (data) {
            notification$.next({
                type: 'success',
                message: 'User created successfully'
            });
            navigate('/');
        }
    // eslint-disable-next-line
    }, [data, error]);
    
    return (
        <>
            <div className={`${block}__left-door`}></div>
            <div className={`${block}__right-door`}></div>
            <form method="post" className={`${block}__form`}>
                <h1 className={`${block}__title`}>Sign Up</h1>
                <Link 
                to='/' 
                aria-label='Go back to the landing page'
                className={`${block}__go-back-btn`}>
                    Go back
                </Link>
                { (pending && <Loader/>) || 
                    <>
                        <div className={`${block}__personal-info-sect`}>
                            <h2>Personal Information</h2>
                            <TextInput 
                                name={'full_name'}
                                label={'Full name'}
                                type={'text'}
                                placeholder={'John Doe'}
                                initialValue={formData.full_name}
                                onChange={onChange}
                                hasError={formErrors.full_name}
                                errorMessage={errorMessages.full_name}/>
                            <TextInput 
                                name={'id'}
                                label={'ID number'}
                                type={'text'}
                                placeholder={'112347890'}
                                initialValue={formData.id}
                                onChange={onChange}
                                hasError={formErrors.id}
                                errorMessage={errorMessages.id}/>
                            <SelectInput 
                                options={[
                                    {name: 'Employed/Salaried', value: 'employed_salaried'},
                                    {name: 'Business Owner', value: 'business_owner'},
                                    {name: 'Self-Employed', value: 'self_employed'},
                                    {name: 'Retired', value: 'retired'},
                                    {name: 'Investor', value: 'investor'},
                                    {name: 'Other', value: 'other'}
                                ]}
                                name={'source_income'}
                                label={'Source of Income'}
                                onChange={onChange}/>
                            <FileInput 
                                name={'id_photo'}
                                label={'ID photo'}
                                onUpload={onFileUpload}/>
                        </div>
                        <div className={`${block}__account-info-sect`}>
                            <h2>Account Information</h2>
                            <TextInput 
                                name={'email'} 
                                label={'Email'} 
                                type={'text'} 
                                placeholder={'Input your email'}
                                initialValue={formData.email}
                                onChange={onChange}
                                hasError={formErrors.email}
                                errorMessage={errorMessages.email}/>
                            <TextInput 
                                name={'password'} 
                                label={'Password'} 
                                type={'password'} 
                                placeholder={'Input your password'}
                                onChange={onChange}
                                hasError={formErrors.password}
                                errorMessage={errorMessages.password}/>
                            <TextInput 
                                name={'confirm'} 
                                label={'Confirm password'} 
                                type={'password'} 
                                instruction={'Must match password'}
                                onChange={onChange}
                                hasError={formErrors.confirm}
                                errorMessage={errorMessages.confirm}/>
                        </div>
                        <div className={`${block}__submit-sect`}>
                            <button type="submit" onClick={onSubmit} className={`${block}__submit-btn`}>
                                Create account
                            </button>
                        </div>
                    </>
                }
            </form>
        </>
    )
}

export default SignUpForm;