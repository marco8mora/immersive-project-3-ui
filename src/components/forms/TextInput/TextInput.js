import { useState } from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";

const TextInput = props => {
    const block = 'text-input';
    const { name, label, type, placeholder, 
        initialValue, instruction, onChange,
        hasError, errorMessage } = props;
    const [ showPass, setShowPass ] = useState(false);
    
    return ( 
        <div className={`${block}__root ` +
        `${(instruction || errorMessage) ? block + '__root--with-margin':''}`}>
            <label id={`${name}Label`} htmlFor={`${name}Input`} className={`${block}__label`}>{label}</label>
            <span className={`${block}__input-container`}>
                <input 
                type={
                    (type === 'password' && (showPass ? 'text' : 'password'))
                    ||
                    type
                } 
                name={name} 
                id={`${name}Input`} 
                placeholder={placeholder}
                defaultValue={initialValue}
                aria-labelledby={`${name}Label`}
                aria-describedby={
                    `${instruction ? name + 'Instruction' : ''} ` +
                    `${hasError ? name + 'Error' : ''} `
                }
                className={`
                    ${block}__input  
                    ${hasError ? block + '__input--error':''}`}
                onChange={event => onChange(event)}/>
                {   type === 'password' &&
                    <button type="button" className={`${block}__show-pass-btn`}
                    onClick={()=>setShowPass(curr => !curr)}
                    aria-label={showPass ? 'Hide password' : 'Show password'}
                    >
                        {showPass ? <FaEyeSlash/> : <FaEye />}
                    </button>
                }
                {   
                    ((errorMessage && hasError) &&
                        <span id={`${name}Error`} className={`${block}__error`}>{errorMessage}</span>)
                    ||
                    (instruction &&
                        <span id={`${name}Instruction`} className={`${block}__instruction`}>{instruction}</span>)
                }
            </span>
        </div>
    );
}

export default TextInput;