import { ReactComponent as CoinSVG } from "../../../assets/vectors/coin.svg";

const Coin = () => {
    const block = 'sliding-coin';

    return (
        <div className={`${block}__root`}>
            <CoinSVG/>
        </div>
    );
}

export default Coin;