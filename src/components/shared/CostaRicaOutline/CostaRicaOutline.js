import { ReactComponent as CostaRicaOutlineSVG } from "../../../assets/vectors/costaRicaOutline.svg";

const CostaRicaOutline = () => {
  return (
    <>
      <CostaRicaOutlineSVG/>
    </>
  )
}

export default CostaRicaOutline;