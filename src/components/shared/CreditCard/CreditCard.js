import FullLogo from "../FullLogo/FullLogo";
import Visa from "../Visa/Visa";

const CreditCard = props => {
    const block = 'credit-card';
    const { accountNumber, currencyCode, design } = props;

    return (
        <div className={`${block}__root ${block}__root--${design}`}>
            <div className={`${block}__logo-wrp`}>
                <FullLogo/>
            </div>
            <div className={`${block}__number-wrp`}>
                <data className={`${block}__number`}>{accountNumber}</data>
            </div>
            <div className={`${block}__currency-wrp`}>
                <data className={`${block}__number`}>{currencyCode}</data>
            </div>
            <div className={`${block}__visa-wrp`}>
                <Visa />
            </div>
        </div>
    )
}

export default CreditCard;