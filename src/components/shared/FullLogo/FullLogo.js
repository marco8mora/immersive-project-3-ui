import { ReactComponent as FullLogoSVG } from "../../../assets/vectors/fullLogo.svg";

const FullLogo = () => {
  return (
    <>
      <FullLogoSVG />
    </>
  )
}

export default FullLogo;