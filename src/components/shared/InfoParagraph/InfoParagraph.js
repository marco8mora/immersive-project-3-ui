const InfoParagraph = props => {
    const block = 'info-paragraph'
    const { title, info, textColor } = props

    return (
        <div className={`${block}__root ${block}__root--${textColor}`}>
            <h3 className={`${block}__title`}>{title}</h3>
            <p className={`${block}__info`}>{info}</p>
        </div>
    )
}

export default InfoParagraph