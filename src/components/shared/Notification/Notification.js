import { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import useNotification from "../../../hooks/useNotification";
import useToggleBodyScroll from '../../../hooks/useToggleBodyScroll';

const Notification = () => {
    const block = 'notification';
    const { notification, notification$ } = useNotification(null);
    const toggleBodyScroll = useToggleBodyScroll();
    const rootRef = useRef(null);
    
    const closeNotification = () => {
        notification$.next(null);
        toggleBodyScroll();
    }

    useEffect(() => {
        if(notification) {
            rootRef.current.focus();
            toggleBodyScroll();
        }
        // eslint-disable-next-line
    }, [notification]);

    return ReactDOM.createPortal(
        <>
            {   notification &&
                <>
                    <div className={`${block}__overlay`}/>
                    <div className={`${block}__root ${block}__root--${notification.type}`}
                    tabIndex='0'
                    aria-label='Notification appeared on screen'
                    ref={rootRef}
                    >
                        <p className={`${block}__message`}>
                            {notification.message}
                        </p>
                        <button className={`${block}__close-btn`} 
                        onClick={closeNotification}
                        aria-label='Close notification'>
                            Close
                        </button>
                    </div>
                </>
            }
        </>,
        document.getElementById('notification')
    );
}

export default Notification;