import { ReactComponent as BorderSVG } from "../../../assets/vectors/border.svg";

const RotatingBorder = () => {
  return (
    <>
        <BorderSVG/>
    </>
  )
}

export default RotatingBorder;