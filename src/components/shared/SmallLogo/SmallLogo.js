import { ReactComponent as SmallLogoSVG } from "../../../assets/vectors/smallLogo.svg";

const SmallLogo = () => {
  return (
    <>
      <SmallLogoSVG />
    </>
  )
}

export default SmallLogo;