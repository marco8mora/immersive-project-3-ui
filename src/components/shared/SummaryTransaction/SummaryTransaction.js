import { useEffect, useState } from "react";
import useFetch from "../../../hooks/useFetch";

const SummaryTransaction = props => {
  const block = 'summary-transaction';
  const { accountNumber, symbol, transaction } = props;
  const [displayedAmount, setDisplayedAmount] = useState(transaction.amount);
  
  const creditedCurrency = transaction.account_credited.slice(0,2) === 'CR' ? 'crc' : 'usd';
  const debitedCurrency = transaction.account_debited ? transaction.account_debited.slice(0,2) === 'CR' ? 'crc' : 'usd' : creditedCurrency;
  const accountIsDebited = accountNumber === transaction.account_debited;
  const differentCurrencies = creditedCurrency !== debitedCurrency;

  let exchangeData = {}
  if (differentCurrencies && accountIsDebited) {
    exchangeData = {
      from: creditedCurrency,
      to: debitedCurrency,
      amount: transaction.amount
    }
  }

  const [ data, error, sendRequest ] = useFetch(
    process.env.REACT_APP_API_URL + `/exchange`,
    false,
    {
      method: 'POST',
      credentials: "include",
      headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(exchangeData)
    }
  );

  useEffect(() => {
    if(differentCurrencies && accountIsDebited) {
      sendRequest();
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (differentCurrencies && accountIsDebited) {
      if(data) {
        setDisplayedAmount(data.data)
      }
    }
    // eslint-disable-next-line
  }, [data, error]);

  return (
    <div className={`${block}__root`}>
      <p className={`${block}__description`}>
        {transaction.description}
      </p>
      <data className={`${block}__amount 
      ${accountIsDebited ? block + '__amount--debited' : block + '__amount--credited' }`}>
        { accountIsDebited ?
          <>-</> : <>+</>
        }
        {symbol}
        {displayedAmount}
      </data>
    </div>
  )
}

export default SummaryTransaction;