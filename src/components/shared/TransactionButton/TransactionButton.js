import { useEffect, useState } from "react";
import useFetch from "../../../hooks/useFetch";

const TransactionButton = props => {
    const block = 'transaction-button';
    const { accountNumber, transaction, symbol, onClick } = props;
    const [displayedAmount, setDisplayedAmount] = useState(transaction.amount);

    const creditedCurrency = transaction.account_credited.slice(0,2) === 'CR' ? 'crc' : 'usd';
    const debitedCurrency = transaction.account_debited ? transaction.account_debited.slice(0,2) === 'CR' ? 'crc' : 'usd' : creditedCurrency;
    const accountIsDebited = accountNumber === transaction.account_debited;
    const differentCurrencies = creditedCurrency !== debitedCurrency;

    let exchangeData = {}
    if (differentCurrencies && accountIsDebited) {
        exchangeData = {
        from: creditedCurrency,
        to: debitedCurrency,
        amount: transaction.amount
        }
    }

    const [ data, error, sendRequest ] = useFetch(
        process.env.REACT_APP_API_URL + `/exchange`,
        false,
        {
          method: 'POST',
          credentials: "include",
          headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Credentials': 'true'
          },
          body: JSON.stringify(exchangeData)
        }
      );
    
    useEffect(() => {
    if(differentCurrencies && accountIsDebited) {
        sendRequest();
    }
    // eslint-disable-next-line
    }, []);

    useEffect(() => {
    if (differentCurrencies && accountIsDebited) {
        if(data) {
        setDisplayedAmount(data.data)
        }
    }
    // eslint-disable-next-line
    }, [data, error]);
  
    return (
        <button type="button" className={`${block}__root`}
            aria-label={`Display details for trasaction done on ${new Date(transaction.created_at).toLocaleString()} with description: ${transaction.description}`}
            onClick={onClick}>
            <div className={`${block}__info-section`}>
                <div className={`${block}__info-section__date`}>
                    <p>{new Date(transaction.created_at).toLocaleString()}</p>
                </div>
                <div className={`${block}__info-section__description`}>
                    <p>{transaction.description}</p> 
                </div>
            </div>
            <div className={`${block}__amount`}>
                <data>
                    { accountIsDebited ?
                        <>-</> : <>+</>
                    }
                    {symbol}
                    {displayedAmount}
                </data>
            </div>
        </button>
    );
}

export default TransactionButton