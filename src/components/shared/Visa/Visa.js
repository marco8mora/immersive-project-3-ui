import { ReactComponent as VisaSVG } from "../../../assets/vectors/visa.svg";

const Visa = () => {
    return (
        <>
            <VisaSVG />
        </>
    )
}

export default Visa;