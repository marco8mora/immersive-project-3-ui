import React, { useState, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Loader from '../components/shared/Loader/Loader';
import useFetch from '../hooks/useFetch';
import useNotification from '../hooks/useNotification';

const WhoIsAuthorizeContext = React.createContext();
const SetWhoIsAuthorizeContext = React.createContext();

export const useWhoIsAuthorize = () => {
    return useContext(WhoIsAuthorizeContext);
}

export const useSetWhoIsAuthorize = () => {
    return useContext(SetWhoIsAuthorizeContext);
}

export const AuthorizationProvider = ({children}) => {
    const [ whoIsAuthorize, setWhoIsAuthorize ] = useState(undefined);
    const navigate = useNavigate();
    const { notification$ } = useNotification();
    const [ data, error, sendRequest, pending ] = useFetch(process.env.REACT_APP_API_URL + '/verify',
    false,
    {
        credentials: "include",
    });

    useEffect(() => {
        const cookies = new Cookies();
        const authToken = cookies.get('auth_token');
        if(authToken) {
            sendRequest();
        } else {
            setWhoIsAuthorize(null);
        }
    // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if(error) {
            if(error.name === 'TypeError') {
                notification$.next({
                    type: 'error',
                    message: "Couldn't communicate with the server, try again later."
                });
            } else if(error.cause.status === 401) {
                if (error.cause.name === 'InvalidTokenError') {
                    const cookies = new Cookies();
                    cookies.remove('auth_token');
                    notification$.next({
                        type: 'error',
                        message: "Session expired. Log in again."
                    });
                }
            } else {
                notification$.next({
                    type: 'error',
                    message: 'The server could not process the request, please try again later.'
                });
            }
            setWhoIsAuthorize(null);
            navigate('/');
        } else if (data) {
            const cookies = new Cookies();
            setWhoIsAuthorize(cookies.get('auth_token'));
            navigate('/user/dashboard');
        }
    // eslint-disable-next-line
    }, [data, error]);

    return ( 
        <> 
            {   (pending && <Loader/>) ||
                <SetWhoIsAuthorizeContext.Provider value={setWhoIsAuthorize}>
                    <WhoIsAuthorizeContext.Provider value={whoIsAuthorize}>
                        {children}
                    </WhoIsAuthorizeContext.Provider>
                </SetWhoIsAuthorizeContext.Provider>
            }
        </>
    );
};