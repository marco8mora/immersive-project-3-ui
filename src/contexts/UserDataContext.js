import React, { useState, useContext } from 'react';

const UserDataContext = React.createContext();
const SetUserDataContext = React.createContext();

export const useUserData = () => {
    return useContext(UserDataContext);
}

export const useSetUserData = () => {
    return useContext(SetUserDataContext);
}

export const UserDataProvider = ({children}) => {
    const [ userData, setUserData ] = useState({});

    return ( 
        <SetUserDataContext.Provider value={setUserData}>
            <UserDataContext.Provider value={userData}>
                {children}
            </UserDataContext.Provider>
        </SetUserDataContext.Provider>
    );
}

