import { useEffect, useState } from "react";

const useAnimationSetter = (elements) => {
    const [playAnimation, setPlayAnimation] = useState(false);
    
    const play = () => setPlayAnimation(true);

    useEffect(() => {
        let timerIds;
        if(playAnimation) {
            timerIds = [];
            elements.forEach(element => {
                const timer = setTimeout(() => {
                    element.ref.current.classList.add(element.class);
                }, element.delay);
                timerIds.push(timer);
            });
        }

        return () => {
            timerIds && timerIds.forEach(timer => {
                clearTimeout(timer);
            });
        }
    // eslint-disable-next-line
    }, [playAnimation]);

    return play;
};

export default useAnimationSetter;