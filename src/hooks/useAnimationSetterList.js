import { useEffect, useState } from "react";

const useAnimationSetterList = (elements, className, delay) => {
    const [playAnimation, setPlayAnimation] = useState(false);
    
    const play = () => setPlayAnimation(true);

    useEffect(() => {
        let timerIds;
        if(playAnimation) {
            timerIds = [];
            elements.forEach((element, index) => {
                const timer = setTimeout(() => {
                    element.classList.add(className);
                }, (index+1) * delay);
                timerIds.push(timer);
            });
        }

        return () => {
            timerIds && timerIds.forEach(timer => {
                clearTimeout(timer);
            });
        }
    // eslint-disable-next-line
    }, [playAnimation]);

    return play;
};

export default useAnimationSetterList;