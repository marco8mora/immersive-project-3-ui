import { useState, useEffect } from "react";

let breakpoints = {
    "tablet": 600,
    "desktop": 900,
    "lg-desktop": 1200
}

const useBreakpoint = () => {
    const [windowWidth, setWindowWidth] = useState(document.documentElement.clientWidth);

    useEffect(() => {
        const onResize = () => setWindowWidth(document.documentElement.clientWidth);
        window.addEventListener("resize", onResize);
        return () => window.removeEventListener('resize', onResize); 
    }, []);
     
    let device = "mobile"
    if (windowWidth >= breakpoints["tablet"] && windowWidth < breakpoints["desktop"]) {
        device = "tablet";
    } else if (windowWidth >= breakpoints["desktop"] && windowWidth < breakpoints["lg-desktop"]) {
        device = "desktop" ;
    } else if (windowWidth >= breakpoints["lg-desktop"]) {
        device = "lg-desktop";
    }

    return device;
}

export default useBreakpoint;