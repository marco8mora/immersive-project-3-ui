import { useState, useEffect } from "react";

const useFetch = (url, initialSend, options = null) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [canSend, setCanSend] = useState(initialSend);
    const [pending, setPending] = useState(false);
    
    const sendRequest = () => setCanSend(true);

    useEffect(() => {
        if(canSend) {
            setPending(true);
            fetch(url, options)
                .then( async response => {
                    if(response.status !== 200) {
                        const errorData = await response.json();
                        throw new Error(errorData.error.message,
                            {cause:
                                {
                                    status: response.status, 
                                    name: errorData.error.name,
                                    data: errorData.data
                                }
                            }
                        );
                    } else {
                        return response.json();
                    }                    
                })
                .then(parsedData =>  {
                    setData(parsedData);
                    setError(null);
                    setPending(false);
                })
                .catch(error => {
                    setError(error);
                    setPending(false);
                });
            
            setCanSend(false);
        }
    }, [url, options, canSend]);
    
    return [ data, error, sendRequest, pending ];
}

export default useFetch;