import { useEffect, useState } from "react";
import { BehaviorSubject } from "rxjs";

let notification$ = null;

const useNotification = () => {
    const [ notification, setNotification ] = useState();

    if (!notification$) { notification$ = new BehaviorSubject(null) }
    
    useEffect(() => {
        let sub = notification$.subscribe(newNotification => {
            setNotification(newNotification);
        });

        return () => sub.unsubscribe();
    }, []);

    return { notification , notification$ };
};

export default useNotification;