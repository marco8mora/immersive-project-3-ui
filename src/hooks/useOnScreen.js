import { useState, useEffect } from "react";

const useOnScreen = (ref, threshold = 0) => {
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {
                setIsVisible(entry.isIntersecting);
            },{
                threshold: threshold
            }
        );
    
        const currentElement = ref?.current;
    
        if (currentElement) {
            observer.observe(currentElement);
        }
    
        return () => {
            observer.unobserve(currentElement);
        };
    }, [ref, threshold]);

    return isVisible;
}

export default useOnScreen;