import { useState, useEffect } from "react"

const DIRECTIONS = {
    UP: 'up',
    DOWN: 'down'
}

const useScrollDirection = (threshold = 100) => {
    const [scrollDirection, setScrollDirection] = useState(DIRECTIONS.UP);
  
    useEffect(() => {
        let previousScrollYPosition = window.scrollY;
    
        const scrolledMoreThanThreshold = (currentScrollYPosition) =>
          Math.abs(currentScrollYPosition - previousScrollYPosition) > threshold;
    
        const isScrollingUp = (currentScrollYPosition) =>
          currentScrollYPosition > previousScrollYPosition &&
          !(previousScrollYPosition > 0 && currentScrollYPosition === 0) &&
          !(currentScrollYPosition > 0 && previousScrollYPosition === 0);
    
        const updateScrollDirection = () => {
          const currentScrollYPosition = window.scrollY;
    
          if (scrolledMoreThanThreshold(currentScrollYPosition)) {
            const newScrollDirection = isScrollingUp(currentScrollYPosition)
              ? DIRECTIONS.DOWN
              : DIRECTIONS.UP;
              setScrollDirection(newScrollDirection);
            previousScrollYPosition =
              currentScrollYPosition > 0 ? currentScrollYPosition : 0;
          }
        };
    
        const onScroll = () => window.requestAnimationFrame(updateScrollDirection);
    
        window.addEventListener("scroll", onScroll);
    
        return () => window.removeEventListener("scroll", onScroll);
      }, [threshold]);
  
    return scrollDirection;
};

export default useScrollDirection;