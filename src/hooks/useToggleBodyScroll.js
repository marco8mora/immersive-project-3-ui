import { useEffect, useState } from "react";

const useToggleBodyScroll = () => {
    const [ canScroll, setCanScroll ] = useState(true);

    const toggleBodyScroll = () => setCanScroll(curr => !curr);

    useEffect(() => {
        if(canScroll) {
            document.body.classList.remove("unscrollable")
        } else {
            document.body.classList.add("unscrollable");
        }
    }, [canScroll]);

    return toggleBodyScroll;
};

export default useToggleBodyScroll;