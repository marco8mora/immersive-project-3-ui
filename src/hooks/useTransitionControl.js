import { useState, useEffect } from "react";

const STATE = {
    ENTERING: 'entering',
    ENTERED: 'entered',
    EXITING: 'exiting',
    EXITED: 'exited'
};

const useTransitionState = (duration = 1000) => {
    const [transitionState, setTransitionState] = useState();

    useEffect(() => {
        let timerId;
        if(transitionState === STATE.ENTERING) {
            timerId = setTimeout(() => setTransitionState(STATE.ENTERED), duration);
        } else if (transitionState === STATE.EXITING) {
            timerId = setTimeout(() => setTransitionState(STATE.EXITED), duration);
        }

        return () => {
            timerId && clearTimeout(timerId);
        };
    });

    return [transitionState, setTransitionState];
};

const useTransitionControl = (duration) => {
    const [transitionState, setTransitionState] = useTransitionState(duration);

    const enter = () => {
        if(transitionState !== STATE.EXITING) {
            setTransitionState(STATE.ENTERING);
        }
    };

    const exit = () => {
        if(transitionState !== STATE.ENTERING) {
            setTransitionState(STATE.EXITING);
        }
    };

    return [transitionState, enter, exit];
}

export default useTransitionControl;