import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.scss';
import App from './components/App/App';
import UserNav from './components/UserNav/UserNav';
import Loader from './components/shared/Loader/Loader';
import Notification from './components/shared/Notification/Notification';
import { AuthorizationProvider } from './contexts/AuthorizationContext';
const Landing = lazy(() => import('./pages/Landing/Landing'));
const SignUp = lazy(() => import('./pages/SignUp/SignUp'));
const Dashboard = lazy(() => import('./pages/Dashboard/Dashboard'));
const History = lazy(() => import('./pages/History/History'));
const Profile = lazy(() => import('./pages/Profile/Profile'));
const AddMoney = lazy(()=> import('./pages/AddMoney/AddMoney'));
const Transfer = lazy(() => import('./pages/Transfer/Transfer'));
const Services = lazy(() => import('./pages/Services/Services'));
const NotFound = lazy(() => import('./pages/NotFound/NotFound'));

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Notification/>
      <AuthorizationProvider>
        <Suspense fallback={<Loader/>}>
          <Routes>
            <Route path="/" element={<App />}>
              <Route index element={<Landing />}/>
              <Route path="/signup" element={<SignUp />}/>
            </Route>
            <Route path="/user" element={<UserNav />}>
              <Route path="/user/dashboard" element={<Dashboard />}/>  
              <Route path="/user/history/:account" element={<History />}/>
              <Route path="/user/profile" element={<Profile />}/>
              <Route path="/user/addmoney" element={<AddMoney />}/>
              <Route path="/user/transfer" element={<Transfer />}/>
              <Route path="/user/services" element={<Services />}/>
            </Route>
            <Route path="*" element={<NotFound />}/>
          </Routes>
        </Suspense>
      </AuthorizationProvider>
    </BrowserRouter>
  </React.StrictMode>
);