import AddMoneyForm from "../../components/forms/AddMoneyForm/AddMoneyForm";
import Loader from "../../components/shared/Loader/Loader";
import { useUserData } from "../../contexts/UserDataContext";

const AddMoney = () => {
  const block = 'add-money';
  const userData = useUserData();

  return (
    <div className={`${block}__root`}>
      <h1 className={`${block}__title`}>Add money to account</h1>
      { (!userData.accounts && <Loader />) || 
        <div className={`${block}__container`}>
          <AddMoneyForm accounts={userData.accounts} />
        </div>
      }
    </div>
  )
}

export default AddMoney;