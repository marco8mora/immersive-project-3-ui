import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import useFetch from "../../hooks/useFetch";
import useNotification from "../../hooks/useNotification";
import { useUserData, useSetUserData } from "../../contexts/UserDataContext";
import { useSetWhoIsAuthorize } from "../../contexts/AuthorizationContext";
import useBreakpoint from "../../hooks/useBreakpoint";
import CardPanel from "../../components/CardPanel/CardPanel";
import Loader from "../../components/shared/Loader/Loader";
import Cookies from "universal-cookie";

const Dashboard = () => {
  const block = 'dashboard';
  const navigate = useNavigate();
  const { notification$ } = useNotification();
  const setWhoIsAuthorize = useSetWhoIsAuthorize();
  const userData = useUserData();
  const setUserData = useSetUserData();
  const device = useBreakpoint();
  const [ displayedAccount, setDisplayedAccount ] = useState('crc');
  const [ userDataRes, userErrorRes, 
    userSendRequest ] = useFetch(process.env.REACT_APP_API_URL + '/user',
    false,
    {
      credentials: "include",
  });

  const [ accountsDataRes, accountsErrorRes , 
    accountsSendRequest ] = useFetch(process.env.REACT_APP_API_URL + '/accounts/user',
    false,
    {
      credentials: "include",
  });

  useEffect(() => {
    if(!userData.user) {
      userSendRequest();
    }
    if(!userData.accounts) {
      accountsSendRequest();
    }
    // eslint-disable-next-line
  }, [userData]);

  useEffect(() => {
    if(userErrorRes) {
      if(userErrorRes.name === 'TypeError') {
          notification$.next({
              type: 'error',
              message: "Couldn't communicate with the server, try again later."
          });
      } else if(userErrorRes.cause.status === 401) {
          if(userErrorRes.cause.name === 'InvalidTokenError') {
            const cookies = new Cookies();
            cookies.remove('auth_token');
            notification$.next({
              type: 'error',
              message: "Session expired. Log in again"
            });
          } else {
            notification$.next({
              type: 'error',
              message: "You must be logged in to access this page"
            });
          }
          setWhoIsAuthorize(null);
          navigate('/');
      } else if (userErrorRes.cause.status === 404) {
          navigate('/user/notfound');
      } else {
          notification$.next({
              type: 'error',
              message: 'The server could not process the request, please try again later.'
          });
      }
    } else if (userDataRes) {
      setUserData(curr => { return {
        ...curr,
        user: userDataRes.data
      }});
    }
    // eslint-disable-next-line
  }, [userDataRes, userErrorRes]);

  useEffect(() => {
    if(accountsErrorRes) {
      if(accountsErrorRes.name === 'TypeError') {
          notification$.next({
              type: 'error',
              message: "Couldn't communicate with the server, try again later."
          });
      } else if(accountsErrorRes.cause.status === 401) {
          if(accountsErrorRes.cause.name === 'InvalidTokenError') {
            const cookies = new Cookies();
            cookies.remove('auth_token');
            notification$.next({
              type: 'error',
              message: "Session expired. Log in again"
            });
          } else {
            notification$.next({
              type: 'error',
              message: "You must be logged in to access this page"
            });
          }
          setWhoIsAuthorize(null);
          navigate('/');
      } else if (accountsErrorRes.cause.status === 404) {
          navigate('/user/notfound');
      } else {
          notification$.next({
              type: 'error',
              message: 'The server could not process the request, please try again later.'
          });
      }
    } else if (accountsDataRes) {
      setUserData(curr => { return {
        ...curr,
        accounts: accountsDataRes.data
      }});
    }
    // eslint-disable-next-line
  }, [accountsDataRes, accountsErrorRes]);

  const showMobile = device === 'mobile' || device === 'tablet'

  return (
    <div className={`${block}__root`}>
      <h1 className={`${block}__title`}>Dashboard</h1>
      { (!userData.user && <Loader />) ||
        <>
          <h2 className={`${block}__greeting`}>
            <span className={`${block}__greeting__welcome`}>Welcome,</span>
            <span className={`${block}__greeting__name`}>
              <Link to="/user/profile" aria-label="Go to the profile page">
                {userData.user.full_name}
              </Link>
            </span>
          </h2>
          { showMobile &&
            <button 
            onClick={() => setDisplayedAccount(curr => curr === 'crc' ? 'usd' : 'crc')}
            className={`${block}__next-btn`}>
              Next account
            </button>
          }
          { (!userData.accounts && <Loader />) ||
            <>
              { (showMobile && 
                <>
                  { displayedAccount === 'crc' &&
                    <CardPanel 
                      account={userData.accounts.find(account => account.currency === 'crc')}  
                      design={'blue-yellow'}/>  
                  }
                  { displayedAccount === 'usd' &&
                    <CardPanel 
                      account={userData.accounts.find(account => account.currency === 'usd')}  
                      design={'blue-yellow'}/>
                  }
                </>) ||
                <>
                  <CardPanel 
                    account={userData.accounts.find(account => account.currency === 'crc')}  
                    design={'blue-yellow'}
                    position={'left'}/>
                  <CardPanel 
                    account={userData.accounts.find(account => account.currency === 'usd')}  
                    design={'blue-yellow'}/>  
                </>
              }
            </>
          }
        </>
      }
    </div>  
  )
}

export default Dashboard;