import { useEffect, useRef, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useSetUserData, useUserData } from '../../contexts/UserDataContext';
import { useSetWhoIsAuthorize } from '../../contexts/AuthorizationContext';
import useFetch from '../../hooks/useFetch';
import useNotification from '../../hooks/useNotification';
import Cookies from 'universal-cookie';
import CreditCard from '../../components/shared/CreditCard/CreditCard';
import Loader from '../../components/shared/Loader/Loader';
import TransactionButton from '../../components/shared/TransactionButton/TransactionButton';
import Modal from '../../components/containers/Modal/Modal';
import {BiSearchAlt} from 'react-icons/bi';
import useToggleBodyScroll from '../../hooks/useToggleBodyScroll';
import DateRangeForm from '../../components/forms/DateRangeForm/DateRangeForm';
import { IoIosClose } from 'react-icons/io';

const CURRENCIES = {
  crc: {code: 'CRC', symbol: '₡'},
  usd: {code: 'USD', symbol: '$'}
}

const History = () => {
  const block = 'history';
  const { account } = useParams();
  const setWhoIsAuthorize = useSetWhoIsAuthorize();
  const navigate = useNavigate();
  const userData = useUserData();
  const setUserData = useSetUserData();
  const { notification$ } = useNotification();
  const [ transactList, setTransactList ] = useState([]);
  const [ isModalOpen, setIsModalOpen ] = useState(false);
  const [ displayedDetails, setDisplayedDetails ] = useState(null);
  const toggleBodyScroll = useToggleBodyScroll();
  const closeDetailsRef = useRef(null);

  const [ accountData, accountDataError, accountSendRequest ] = useFetch(
    process.env.REACT_APP_API_URL + `/accounts/${account}`,
    false,
    {
      credentials: "include",
  });
  const [ transactData, transactError, transactSendRequest, transactPending ] = useFetch(
    process.env.REACT_APP_API_URL + `/transactions/${account}?limit=99&from=${new Date().toJSON()}&to=${new Date().toJSON()}`,
    false,
    {
      credentials: "include",
  });

  useEffect(() => {
    const accountToDisplay = userData.accounts.find(acc => acc.number === account);
    if(!accountToDisplay) {
      accountSendRequest();
    } 
    
    transactSendRequest();
    // eslint-disable-next-line
  }, [userData]);

  useEffect(() => {
    if(accountDataError) {
      if(accountDataError.name === 'TypeError') {
          notification$.next({
              type: 'error',
              message: "Couldn't communicate with the server, try again later."
          });
      } else if(accountDataError.cause.status === 401) {
          if(accountDataError.cause.name === 'InvalidTokenError') {
            const cookies = new Cookies();
            cookies.remove('auth_token');
            notification$.next({
              type: 'error',
              message: "Session expired. Log in again"
            });
          } else {
            notification$.next({
              type: 'error',
              message: "You must be logged in to access this page"
            });
          }
          setWhoIsAuthorize(null);
          navigate('/');
      } else if (accountDataError.cause.status === 404) {
          navigate('/user/notfound');
      } else {
          notification$.next({
              type: 'error',
              message: 'The server could not process the request, please try again later.'
          });
      }
    } else if (accountData) {
      setUserData(curr => { return {
        ...curr,
        accounts: curr.accounts ? [
          ...curr.accounts,
          accountData.data
        ] : [account.data]
      }});
    }
    // eslint-disable-next-line
  }, [accountData, accountDataError]);

  useEffect(() => {
    if(transactError) {
      if(transactError.name === 'TypeError') {
          notification$.next({
              type: 'error',
              message: "Couldn't communicate with the server, try again later."
          });
      } else if(transactError.cause.status === 401) {
          if(transactError.cause.name === 'InvalidTokenError') {
            const cookies = new Cookies();
            cookies.remove('auth_token');
            notification$.next({
              type: 'error',
              message: "Session expired. Log in again"
            });
          } else {
            notification$.next({
              type: 'error',
              message: "You must be logged in to access this page"
            });
          }
          setWhoIsAuthorize(null);
          navigate('/');
      } else if (transactError.cause.status === 404) {
          navigate('/user/notfound');
      } else {
          notification$.next({
              type: 'error',
              message: 'The server could not process the request, please try again later.'
          });
      }
    } else if (transactData) {
      setTransactList(transactData.data);
    }
    // eslint-disable-next-line
  }, [transactData, transactError]);

  const displayedAccount = userData.accounts.find(acc => acc.number === account);

  const transactButtons = transactList.map(transact =>
    <li key={transact.id}>
      <TransactionButton 
      accountNumber={displayedAccount.number} 
      transaction={transact}
      symbol={CURRENCIES[displayedAccount.currency].symbol}
      onClick={() => {
        setDisplayedDetails(transact)
        closeDetailsRef.current.focus();
        }}/>
    </li>  
  );

  return (
    <div className={`${block}__root`}>
      <h1 className={`${block}__title`}>Account History</h1>
      { (!displayedAccount && <Loader/>) ||
        <>
          <div className={`${block}__container`}>
            <div className={`${block}__container__card-wrp`}>
              <CreditCard accountNumber={displayedAccount.number} 
              currencyCode={CURRENCIES[displayedAccount.currency].code} 
              design={'blue-yellow'}/>
            </div>
            <div className={`${block}__transcat-list-cont`}>
              <h2 className={`${block}__transcat-list-cont__title`}>Transactions</h2>
              <button className={`${block}__transcat-list-cont__open-dates-btn`}
              aria-label="Open form to input a date range"
              onClick={() => setIsModalOpen(true)}>
                Filter <BiSearchAlt/>
              </button>
              { (transactPending && <Loader/>) ||
                (!transactList.length && <p className={`${block}__transcat-list-cont__no-data`}>No data to display in the selected range</p>) || 
                <ul className={`${block}__transcat-list-cont__list`}>
                  {transactButtons}
                </ul>
              }
            </div>
          </div>
          <Modal open={isModalOpen} onClose={() => {
            setIsModalOpen(false);
            toggleBodyScroll();
          }}>
            <DateRangeForm 
            setList={setTransactList} 
            account={displayedAccount.number}
            closeModal={() => setIsModalOpen(false)}
            />
          </Modal>
        </>
      }
      { displayedDetails &&

        <div className={`${block}__transact-details ${ displayedDetails ? block+'__transact-details--opened':''}`}>
          <h3 className={`${block}__transact-details__title`}>Transaction Details:</h3>
          <button className={`${block}__transact-details__close-btn`}
          ref={closeDetailsRef}
          aria-label='Close transaction details sidebar'
          tabIndex={displayedDetails ? 0 : -1}
          onClick={() => setDisplayedDetails(null)}>
            <IoIosClose/>
          </button>
          <div><h4>Transaction ID: </h4><p>{displayedDetails.id}</p></div>
          <div><h4>Date processed: </h4><p>{new Date(displayedDetails.created_at).toLocaleString()}</p></div>
          <div><h4>Amount: </h4><p>{displayedDetails.amount}</p></div>
          <div><h4>Description: </h4><p>{displayedDetails.description}</p></div>
          { displayedDetails.external_account ?
            <div><h4>Account debited: </h4><p>{displayedDetails.external_account}</p></div> :
            <div><h4>Account debited: </h4><p>{displayedDetails.account_debited}</p></div>
          }
          { displayedDetails.is_service_payment ?
            <div><h4>Service payment: </h4><p>Yes</p></div> :
            <div><h4>Account credited: </h4><p>{displayedDetails.account_credited}</p></div>
          }
        </div>
      }
    </div>
  )
}

export default History;