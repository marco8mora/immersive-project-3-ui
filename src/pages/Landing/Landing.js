import { useEffect, useState } from "react";
import useScrollPosition from "../../hooks/useScrollPosition";
import Hero from "../../components/containers/Hero/Hero";
import Card from "../../components/containers/Card/Card";
import LandingNav from "../../components/LandingNav/LandingNav";
import FullLogo from "../../components/shared/FullLogo/FullLogo";
import Locations from "../../components/Locations/Locations";
import LoginForm from "../../components/forms/LoginForm/LoginForm";
import LandingSection from "../../components/containers/LandingSection/LandingSection";
import OurServices from "../../components/OurServices/OurServices";
import LandingFooter from "../../components/LandingFooter/LandingFooter";

const Landing = () => {
  const block = 'landing';
  const scrollPos = useScrollPosition();
  const [currSection, setCurrSection] = useState();

  const sections = [
    {name: 'Log In', id: 'login', height: 600 , component: 
      <Hero images={[
        'https://img.freepik.com/free-photo/african-american-man-paying-with-credit-card-online-while-making-orders-via-mobile-internet-making-transaction-using-mobile-bank-application_231208-739.jpg?t=st=1656862260~exp=1656862860~hmac=705cc07d2233fbc530505a2be96c670ef3a36af5561a651d620fdb0b70066ede&w=996',
        'https://img.freepik.com/free-photo/businessman-giving-money-his-partner-while-making-contract_1150-37744.jpg?t=st=1656862260~exp=1656862860~hmac=4357fedd73a41322f1d61bada41687841ae1c6542452202248e58fe99eb9f3aa&w=996',
        'https://img.freepik.com/free-photo/cheerful-excited-young-woman-with-mobile-phone-credit-card_231208-10421.jpg?t=st=1656862260~exp=1656862860~hmac=b058f0474f9017ae191556ceede38401ad7f9b6729b2d938bf9906748680dc5a&w=1060',
        'https://img.freepik.com/free-photo/parents-spending-time-with-their-little-girl_23-2149022820.jpg?t=st=1656862465~exp=1656863065~hmac=6afbe52a762ea0b7073cc0ed4c73ea221126ce9cda3394fd0c15b50ccb6cbf93&w=1060',
        'https://img.freepik.com/free-photo/woman-with-luggage-medical-mask-airport-during-pandemic_23-2148789911.jpg?t=st=1656862640~exp=1656863240~hmac=b3cadaf1b5b61fc234fd5399c043c8bdbb4718027f414ce3aea05b12e9786e40&w=1060'
      ]} timeInterval={4000}>
        <div className={`${block}__logo-container`}>
          <FullLogo />
        </div>
        <Card>
          <LoginForm/>
        </Card>
      </Hero>
    },
    {name: 'Our Services', id: 'services', height: 600, component: 
      <LandingSection bgColor='yellow'>
        <OurServices/>
      </LandingSection>
    },
    {name: 'Locations', id: 'locations', component: 
      <LandingSection bgColor='darkblue'>
        <Locations locations={[
          'San José',
          'Cartago',
          'Puerto Limón',
          'Liberia',
          'Jacó'
        ]}/>  
      </LandingSection>
    }
  ]

  useEffect(() => {
    const currSectToScroll = Math.floor((scrollPos + 200)/600);
    if(currSectToScroll !== currSection) {
      setCurrSection(currSectToScroll);
    }
  }, [scrollPos, currSection]);
  
  return (
    <>
      <header>
        <LandingNav sections={sections} currSection={currSection}/>
      </header>
      <main className={`${block}__main`}>
        <h1 className={`${block}__h1`}>Landing Page</h1>
        {
          sections.map(section => 
            <section key={section.id} className={`${block}__section`} id={section.id}>
              {section.component}
            </section>
          )
        }
      </main>
      <LandingFooter/>
    </>
  )
}

export default Landing;