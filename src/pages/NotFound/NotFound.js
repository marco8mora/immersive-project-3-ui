import RotatingBorder from "../../components/shared/RotatingBorder/RotatingBorder";

const NotFound = () => {
  const block = 'not-found';

  return (
    <>
      <main className={`${block}__root`}>
        <h1 className={`${block}__title`}>Not Found</h1>
        <div className={`${block}__border-wrp`}>
          <RotatingBorder/>
        </div>
        <h2 className={`${block}__subtitle`}>The resource you were trying to access wasn't found</h2>
      </main>
    </>
  )
}

export default NotFound;