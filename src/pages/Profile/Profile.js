import { useEffect } from "react";
import { useNavigate } from "react-router";
import Cookies from "universal-cookie";
import useFetch from "../../hooks/useFetch";
import useNotification from "../../hooks/useNotification";
import { useSetUserData, useUserData } from "../../contexts/UserDataContext";
import { useSetWhoIsAuthorize } from "../../contexts/AuthorizationContext";
import Loader from "../../components/shared/Loader/Loader";

const SOURCES = {
  employed_salaried: 'Employed/Salaried',
  business_owner: 'Business Owner',
  self_employed: 'Self-Employed',
  retired: 'Retired',
  investor: 'Investor',
  other: 'Other'
}

const Profile = () => {
  const block = 'profile';
  const userData = useUserData();
  const setUserData = useSetUserData();
  const setWhoIsAuthorize = useSetWhoIsAuthorize();
  const navigate = useNavigate();
  const { notification$ } = useNotification();
  const [ data, error, sendRequest ] = useFetch(process.env.REACT_APP_API_URL + '/user',
    false,
    {
      credentials: "include",
  });

  useEffect(() => {
    if(!userData.user) {
      sendRequest();
    }
    // eslint-disable-next-line
  }, [userData]);

  useEffect(() => {
    if(error) {
      if(error.name === 'TypeError') {
          notification$.next({
              type: 'error',
              message: "Couldn't communicate with the server, try again later."
          });
      } else if(error.cause.status === 401) {
          if(error.cause.name === 'InvalidTokenError') {
            const cookies = new Cookies();
            cookies.remove('auth_token');
            notification$.next({
              type: 'error',
              message: "Session expired. Log in again"
            });
          } else {
            notification$.next({
              type: 'error',
              message: "You must be logged in to access this page"
            });
          }
          setWhoIsAuthorize(null);
          navigate('/');
      } else if (error.cause.status === 404) {
          navigate('/user/notfound');
      } else {
          notification$.next({
              type: 'error',
              message: 'The server could not process the request, please try again later.'
          });
      }
    } else if (data) {
      setUserData(curr => { return {
        ...curr,
        user: data.data
      }});
    }
    // eslint-disable-next-line
  }, [data, error]);
  
  return (
    <div className={`${block}__root`}>
      <h1 className={`${block}__title`}>Profile</h1>
      { (!userData.user && <Loader />) ||
        <div className={`${block}__container`}>
          <h2 className={`${block}__full-name`}>{userData.user.full_name}</h2>
          <div className={`${block}__photo-section`}>
            <h3 className={`${block}__photo-section__photo-label`}>ID Photo:</h3>
            <div className={`${block}__photo-section__photo-cont`}>
              <img src={userData.user.photo_url} alt={`${userData.user.full_name}'s ID`}/>
            </div>
          </div>
          <div className={`${block}__info-section`}>
            <h3>ID: </h3><p>{userData.user.id}</p>
            <h3>Source of Income: </h3><p>{SOURCES[userData.user.source_income]}</p>
            <h3>Email: </h3><p>{userData.user.email}</p>
          </div>
        </div>
      }
    </div>
  )
}

export default Profile;