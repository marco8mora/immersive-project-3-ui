import SignUpForm from "../../components/forms/SignUpForm/SignUpForm";

const SignUp = () => {
    const block = 'signup';
    
    return (
        <main className={`${block}__root`}>
           <SignUpForm/>   
        </main>
    )
}

export default SignUp;